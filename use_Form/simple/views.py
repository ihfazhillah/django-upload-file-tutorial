import base64
from django.shortcuts import render
from django.views.decorators.http import require_POST
from .forms import ImageUploadForm


def index(request):
    form = ImageUploadForm()
    context = {
        'form': form
    }
    return render(request, 'simple/index.html', context)


@require_POST
def handle_simple(request):

    image = request.FILES.get('image')
    error = ''
    image_url = None

    if not image:
        error = 'no image present'

    if not image.name.endswith('.png'): #  just accept the png file
        error = 'the image is not png'

    if image:
        image_url = base64.b64encode(image.read()).decode()

    context = {
        'error': error,
        'image_url': image_url
    }
    return render(request, 'simple/simple.html', context)
