from django.shortcuts import render
from django.views.decorators.http import require_POST
import csv


# Create your views here.
def index(request):
    return render(request, 'simple/index.html')


@require_POST
def handle_simple(request):
    the_file = request.FILES.get('the_file')
    csv_reader = None
    if the_file:
        try:
            the_file_string = the_file.file.read().decode().splitlines()
            csv_reader = csv.reader(the_file_string, delimiter=',')
        except:
            error = 'there is the error occured'
    else:
        error = 'file not found'


    context = {
        'object': csv_reader,
        'error': error
    }
    return render(request, 'simple/simple.html', context)
