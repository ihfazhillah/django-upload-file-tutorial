from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls import reverse

from .forms import TheFileForm
from .models import TheFile

# Create your views here.


def index(request):
    form = TheFileForm()
    files = TheFile.objects.all()
    context = {
        'form': form,
        'files': files
    }
    return render(
        request,
        'app/index.html',
        context
    )


def handle_upload(request):
    form = TheFileForm(request.POST, request.FILES)
    if form.is_valid():
        form.save()
        messages.success(request, 'your file successfuly uploaded')
        return redirect('index')
    messages.error(request, form.errors)
    return redirect('index')


def the_file_detail(request, pk):
    the_file = get_object_or_404(TheFile, pk=pk)
    form = TheFileForm()

    context = {
        'the_file': the_file,
        'form': form
    }

    return render(
        request,
        'app/detail.html',
        context
    )


def handle_the_file_edit(request, pk):
    the_file = get_object_or_404(TheFile, pk=pk)
    form = TheFileForm(
        request.POST,
        request.FILES,
        instance=the_file
    )

    if form.is_valid():
        form.save()
        messages.success(request, 'The file successfully updated')
        return redirect(reverse('detail', args=[pk]))

    messages.error(
        request,
        form.errors
    )

    return redirect(reverse('detail', args=[pk]))


def handle_the_file_delete(request, pk):
    """Handle Delete file
    GET request is to ask confirmation
    POST request is to perform actual deletion

    :request: TODO
    :pk: TODO
    :returns: TODO

    """
    the_file = get_object_or_404(TheFile, pk=pk)

    if request.method == 'POST':
        the_file.delete()
        messages.success(
            request,
            f'The file {the_file.the_file.name} deleted successfully'
        )
        return redirect(reverse('index'))

    context = {
        'file': the_file
    }

    return render(
        request,
        'app/delete.html',
        context
    )
