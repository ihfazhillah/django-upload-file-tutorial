from django.db import models
from django.urls import reverse

# Create your models here.


class TheFile(models.Model):
    the_file = models.FileField(upload_to='files/')

    def get_absolute_url(self):
        return reverse('detail', args=[self.id])

