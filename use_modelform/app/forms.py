from django import forms
from .models import TheFile


class TheFileForm(forms.ModelForm):
    class Meta:
        model = TheFile
        fields = ('the_file',)
