import csv

from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth.models import User

from .forms import CsvUploadForm
from .models import StudentMemorize


# Create your views here.


def index(request):
    form = CsvUploadForm()
    objects = StudentMemorize.objects.all()

    context = {
        'form': form,
        'objects': objects
    }
    return render(
        request,
        'app/index.html',
        context
    )


def handle_upload(request):
    form = CsvUploadForm(request.POST, request.FILES)

    if form.is_valid():
        csv_obj = form.cleaned_data['csv_file']
        for obj in csv_obj:
            student = User.objects.get(username=obj['username'])
            StudentMemorize.objects.create(student=student, subject_id=obj['subject_id'], is_pass=bool(int(obj['is_pass'])))


        messages.success(request, 'Update data from csv success')
        return redirect(reverse('index'))

    messages.error(request, form.errors)
    return redirect(reverse('index'))
