from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Subject(models.Model):
    title = models.CharField(max_length=225)

    def __str__(self):
        return f'<{self.title}>'


class StudentMemorize(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    memorize_at = models.DateTimeField(auto_now_add=True)
    is_pass = models.BooleanField(default=True)

    def __str__(self):
        return f'<{self.student.first_name} - {self.subject}>'
