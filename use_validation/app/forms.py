import csv
from itertools import tee
from django import forms


class CsvUploadForm(forms.Form):
    csv_file = forms.FileField()

    def clean_csv_file(self):
        csv_file = self.cleaned_data['csv_file']

        in_memory_file = csv_file.open()
        try:
            csv_string = str(in_memory_file.read(), 'utf-8')
            csv_list = csv_string.splitlines()

            # not empty
            if not csv_list:
                raise forms.ValidationError('The csv file is empty')

            # validate column length
            for index, obj in enumerate(csv_list):
                is_valid = len(obj.split(',')) == 3
                if not is_valid:
                    raise forms.ValidationError(f'line: {index}, expected column lengths is 3. Found: {len(obj.split(","))}')

            csv_obj = csv.DictReader(
                csv_list,
                fieldnames=['username', 'subject_id', 'is_pass']
            )

            csv_obj, csv_obj_copy = tee(csv_obj)

            # validate column type
            validators = {
                'username': (lambda field: isinstance(field, str), str),
                'subject_id': (lambda field: field.isdigit(), int),
                'is_pass': (lambda field: bool(field), bool)
            }

            for index, obj in enumerate(csv_obj):
                for key, val in obj.items():
                    validator, dtype = validators[key]
                    is_valid = validator(val)

                    if not is_valid:
                        raise forms.ValidationError(f'Line {index}, column {key}. Expected type {dtype}. Found {type(val)}. Current value = "{val}"')

        except UnicodeDecodeError:
            raise forms.ValidationError('Please provide csv file')
        return csv_obj_copy

