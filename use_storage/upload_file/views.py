from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from django.http.response import Http404

from .forms import FileUploadForm

# Create your views here.


def index(request):
    """simple index page for display
    upload form and the file list

    :request: django request object
    :returns: render template

    """
    form = FileUploadForm()

    storage = FileSystemStorage()

    # the storage.listdir return 2-tuple of lists
    # the first is list of directories
    # the second is the list of files of current directory
    files = storage.listdir(storage.location)

    context = {
        'form': form,
        'files': files[1]
    }
    return render(
        request,
        'upload_file/index.html',
        context=context
    )


def handle_upload(request):
    """Handle upload file in django

    :request: TODO
    :returns: TODO

    """
    the_file = request.FILES.get('the_file')

    storage = FileSystemStorage()

    filename = storage.get_available_name(the_file.name)

    storage.save(filename, the_file)

    return redirect('/')


def handle_edit(request, filename):
    """Handle edit file by filename.
    """
    storage = FileSystemStorage()

    if not storage.exists(filename):
        raise Http404('File does not exists')

    if request.method == 'POST':
        the_file = request.FILES.get('the_file')

        storage.delete(filename)

        filename = storage.get_available_name(the_file.name)

        storage.save(filename, the_file)

        return redirect('/')

    form = FileUploadForm()

    context = {
        'form': form,
        'file': filename
    }

    return render(
        request,
        'upload_file/edit.html',
        context
    )


def handle_delete(request, filename):
    """handle delete file

    :request: TODO
    :filename: TODO
    :returns: TODO

    """
    storage = FileSystemStorage()

    if not storage.exists(filename):
        raise Http404('File does not exists')

    storage.delete(filename)
    return redirect('index')
