from django import forms


class FileUploadForm(forms.Form):
    the_file = forms.FileField()
