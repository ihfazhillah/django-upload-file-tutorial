"""use_storage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from upload_file.views import (
    index, handle_upload, handle_edit, handle_delete)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('handle_upload', handle_upload, name='handle_upload'),
    path('handle_edit/<filename>', handle_edit, name='handle_edit'),
    path('handle_delete/<filename>', handle_delete, name='handle_delete')
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
